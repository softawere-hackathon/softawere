#!/usr/bin/python3

import requests
import urllib.request
import urllib.parse
from os import environ
from datetime import datetime
import time
import json
from sty import fg, bg, ef
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway

DOCKER_NETWORK_GATEWAY="172.17.0.1"
BOAVIZTAPI_ENDPOINT = "http://{}:8000".format(DOCKER_NETWORK_GATEWAY)
RUNNER_ENDPOINT = "http://{}:8085".format(DOCKER_NETWORK_GATEWAY)
PUSHGATEWAY_ENDPOINT = "{}:9091".format(DOCKER_NETWORK_GATEWAY)
NOCODB_ENDPOINT="https://db.softaware.hubblo.org"

def main():
    ci_job_started_at = environ.get("CI_JOB_AFTER_REQUIREMENTS_STARTED_AT")
    ci_job_coverage = environ.get("CI_JOB_COVERAGE")
    ci_job_size = environ.get("CI_JOB_SIZE")
    ci_job_test_count = environ.get("CI_JOB_TEST_COUNT")
    ci_project_id = environ.get("CI_PROJECT_ID")
    ci_pipeline_id = environ.get("CI_PIPELINE_ID")
    ci_job_id = environ.get("CI_JOB_ID")

    grafana_link = "https://grafana.eco-qube.eu/d/hzpu8ydVz/ci-cd-impacts?orgId=1&refresh=1m&var-PROJECT_ID={}&var-PIPELINE_ID={}&var-JOB_ID={}".format(ci_project_id, ci_pipeline_id, ci_job_id)

    #ci_job_stopped_at = datetime.now().isoformat()
    ci_job_stopped_at = time.time()
    url = "{}/query?verbose=true&location=NLD&start_time={}&end_time={}".format(
        BOAVIZTAPI_ENDPOINT, ci_job_started_at, ci_job_stopped_at
    )
    f = urllib.request.urlopen(url)
    res = f.read().decode('utf-8')
    d = json.loads(res)
    #push_to_nocodb(d, ci_job_started_at, ci_job_stopped_at, ci_job_coverage, ci_job_size, ci_job_test_count, grafana_link)
    push_to_prom(d, ci_job_started_at, ci_job_stopped_at)
    pretty_print_results(d)
    print_summary(d, grafana_link)

def add_gauge_to_registry(name: str, description: str, value: str, registry: CollectorRegistry):
    g = Gauge(name, description, ['ci_job_id', 'project_name', 'ci_job_name', 'project_id', 'pipeline_id'], registry=registry)
    g.labels(environ.get("CI_JOB_ID"), environ.get("CI_PROJECT_NAME"), environ.get("CI_JOB_NAME"), environ.get("CI_PROJECT_ID"), environ.get("CI_PIPELINE_ID")).set(value)

def push_to_prom(res, started_at, stopped_at):
    registry = CollectorRegistry()
    add_gauge_to_registry(
        name='softawere_job_operational_GWP',
        description="Global Warming Potential: Operational (run) emissions attributed to this CI job. In {}".format(res["total_operational_emissions"]["unit"]),
        value=res["total_operational_emissions"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_embodied_GWP',
        description="Global Warming Potential: Embodied (manufacture, transport, eol) emissions attributed to this CI job. In {}".format(res["embedded_emissions"]["unit"]),
        value=res["embedded_emissions"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_total_GWP',
        description="Global Warming Potential: Total (full lifecycle) emissions attributed to this CI job. In {}".format(res["embedded_emissions"]["unit"]),
        value=res["embedded_emissions"]["value"]+res["total_operational_emissions"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_operational_ADPe',
        description="Abiotic Resources Depletion: Operational (run) minerals consumption attributed to this CI job. In {}".format(res["total_operational_abiotic_resources_depletion"]["unit"]),
        value=res["total_operational_abiotic_resources_depletion"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_embodied_ADPe',
        description="Abiotic Resources Depletion: Embodied (manufacture, transport, eol) minerals consumption attributed to this CI job. In {}".format(res["embedded_abiotic_resources_depletion"]["unit"]),
        value=res["embedded_abiotic_resources_depletion"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_total_ADPe',
        description="Abiotic Resources Depletion: Total (full lifecycle) minerals consumption attributed to this CI job. In {}".format(res["embedded_abiotic_resources_depletion"]["unit"]),
        value=res["embedded_abiotic_resources_depletion"]["value"]+res["total_operational_abiotic_resources_depletion"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_operational_PE',
        description="Primary Energy: Operational (run) primary energy consumption attributed to this CI job. In {}".format(res["total_operational_primary_energy_consumed"]["unit"]),
        value=res["total_operational_primary_energy_consumed"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_embodied_PE',
        description="Primary Energy: Embodied (manufacture, transport, eol) primary energy consumption attributed to this CI job. In {}".format(res["embedded_primary_energy"]["unit"]),
        value=res["embedded_primary_energy"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_total_PE',
        description="Primary Energy: Total (full lifecycle) primary energy consumption attributed to this CI job. In {}".format(res["total_operational_primary_energy_consumed"]["unit"]),
        value=res["embedded_primary_energy"]["value"]+res["total_operational_primary_energy_consumed"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_total_time',
        description="Time needed by the job. In seconds.",
        value=str(float(stopped_at) - float(started_at)),
        registry=registry
    )

    push_to_gateway(PUSHGATEWAY_ENDPOINT, job="from_ci_jobs", registry=registry)

def pretty_print_results(res, level=0):
    indent = ""
    i = 0
    while i < level:
        indent += "\t"
        i+=1
    for k, v in res.items():
        if type(v) is dict:
            print("{}{}{} :".format(indent, fg.red, k))
            pretty_print_results(v, level+1)
        else:
            print("{}{}{} = {}{}".format(indent, fg.white, k, fg.green, v))

def get_nocodb_auth_token() -> str:
    signin_url = NOCODB_ENDPOINT + '/api/v1/auth/user/signin'
    print("Authentication at NocoDB: {}".format(signin_url))
    response = requests.post(signin_url, json={
        'email': environ['NOCODB_USERNAME'],
        'password': environ['NOCODB_PASSWORD']
    }, verify=False)
    response.raise_for_status()
    return response.json().get('token')


def push_to_nocodb(res, started_at, stopped_at, job_coverage, job_size, job_test_count, link):
    token = get_nocodb_auth_token()
    print("NocoDB Token received {}".format(token))
    headers = {'xc-auth': token}
    orgs = 'nc'
    project_name = 'Leaderboard'
    table_name = 'Leaderboard submissions'
    insert_url = NOCODB_ENDPOINT + f'/api/v1/db/data/{orgs}/{project_name}/{table_name}'
    print("NocoDB reporting data to URL: {}".format(insert_url))
    response = requests.post(insert_url, headers=headers, json={
        'Team': environ.get("CI_PROJECT_NAME"),
        'Branch': environ.get("CI_COMMIT_BRANCH"),
        'Submission date': str(datetime.now()),
        'Job ID': environ.get("CI_JOB_ID"),
        'Job duration': float(stopped_at) - float(started_at),
        'Job embedded impact CO2': res["embedded_emissions"]["value"],
        'Job embedded impact ADP': res["embedded_abiotic_resources_depletion"]["value"],
        'Job embedded impact PE': res["embedded_primary_energy"]["value"],
        'Job operational impact CO2': res["total_operational_emissions"]["value"],
        'Emission Factor': float(res["emissions_calculation_data"]["electricity_carbon_intensity"]["value"]),
        'Average Power': float(res["emissions_calculation_data"]["average_power_measured"]["value"]),
        'Grafana Link': link,
        'Test Coverage': job_coverage,
        'Job Size': job_size,
        'Number of Tests': job_test_count,
    }, verify=False)
    if not response.ok:
        print('Error while pushing to nocodb.')

def print_summary(res, grafana_link):
    print(ef.bold)
    print(
        "🏭 {}Global Warming Potential = {}Operational Emissions {}+ {}Embdedded Emissions".format(
            fg.white, fg.grey, fg.white, fg.grey
        )
    )
    print(
        "🏭 {}{}{}{} {} = {}{} {}+ {}{}".format(
            bg.yellow,
            fg.white,
            res["calculated_emissions"]["value"],
            bg.rs,
            res["calculated_emissions"]["unit"],
            fg.grey,
            res["total_operational_emissions"]["value"],
            fg.white,
            fg.grey,
            res["embedded_emissions"]["value"]
        )
    )
    print(
        "⛏️ {}Abiotic Resources Depletion = {}Operational Abiotic Resources depletion {}+ {}Embdedded Abiotic Resources depletion".format(
            fg.white, fg.grey, fg.white, fg.grey
        )
    )
    print(
        "⛏️ {}{}{}{} {} = {}{} {}+ {}{}".format(
            bg.yellow,
            fg.white,
            res["total_operational_abiotic_resources_depletion"]["value"]+res["embedded_abiotic_resources_depletion"]["value"],
            bg.rs,
            res["embedded_abiotic_resources_depletion"]["unit"],
            fg.grey,
            res["total_operational_abiotic_resources_depletion"]["value"],
            fg.white,
            fg.grey,
            res["embedded_abiotic_resources_depletion"]["value"]
        )
    )
    print(
        "🛢️ {}Primary Energy Consumption = {}Operational Primary Energy consumption {}+ {}Embdedded Primary Energy Consumption".format(
            fg.white, fg.grey, fg.white, fg.grey
        )
    )
    print(
        "🛢️ {}{}{}{} {} = {}{} {}+ {}{}".format(
            bg.yellow,
            fg.white,
            res["total_operational_primary_energy_consumed"]["value"]+res["embedded_primary_energy"]["value"],
            bg.rs,
            res["embedded_primary_energy"]["unit"],
            fg.grey,
            res["total_operational_primary_energy_consumed"]["value"],
            fg.white,
            fg.grey,
            res["embedded_primary_energy"]["value"]
        )
    )

    print(
        "🔬 🧪 {}{}Get complete data and analysis at {}{} 🧪 🔬".format(fg.white, bg.yellow, grafana_link, bg.rs)
    )

    print(
            "Aggregated data from all projects tested on this instance are accessible here : {}".format("https://db.softaware.hubblo.org/dashboard/#/nc/view/66a34add-e5f3-4a6a-a1f6-4a42739af7a7")
    )

if __name__ == '__main__':
    main()
